#### Installing and Loading the 'matchbook' Package

Because matchbook is not available via CRAN, but instead available via github, we cannot use `install.packages()` to install the **matchbook** package. We need to install the package via github. Luckily, the function `install_github()` in the **devtools** package will do the trick.

```{r, eval = FALSE, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
library(devtools)
install_github("xanadunf/matchbook")
library(matchbook)
```

```{r, include = FALSE}
library(matchbook)
```

Now that it's installed and loaded, please read the "Quickstart Tutorial" found on the github repository for the **matchbook** package found at [https://github.com/xanadunf/matchbook](https://github.com/xanadunf/matchbook).

#### Logging Into Matchbook.com and Getting Football Games

Since you've read the "Quickstart Tutorial", you should be able to access matchbook.com. So try it yourself before looking at the answer. Go as far as getting the football sport ID after logging in and getting all the event data. (See Solutions)

# 2 Functions
For the next section, the **matchbook** package does not have a couple functions that we need. We need a function that finds only English Premier League games since Matchbook.com offers many other sports as well. We also need a function that maps the team names from the [http://www.football-data.co.uk/](http://www.football-data.co.uk/) data to the team names on Matchbook.com. 

Writing functions is at the center of programming. We want to use R to its fullest potential, and to do that we need to be able to create our own functionalities from scratch. Thus, writing functions, though rather an advanced topic, is crucial to using R well. The next section will go over how to write functions, by using the aforementioned goals as an example. We'll start with some basic functions so you can get familiar with the syntax. Then we'll have you try and write the functions we need to find value bets.

## 2.1 Syntax

The syntax of writing functions is quite intuitive. Like assigning an object a value, we can also assign an object a function. The syntax we use is `function(argument1, argument2, ...){}`. So the arguments inside the brackets are what goes into the function. The things within the curly brackets are the operations the function will go through to give you an output. So let's look at a basic example. Here's a function that raises argument 1 to the power of argument 2.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
exponentialFunction <- function(base, exponent){
  output <- base^exponent
  return(output)
}
exponentialFunction(base = 4, exponent = 3)
```

Note that within the curly brackets, I used the `return()` statement. Note that `return()` is not called a function, though the syntax looks like a function. You must use this statement if you want your function to actually return a value. So for example, if we did not have the `return()` statement at the end, `exponentialFunction()` would be useless.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
exponentialFunction <- function(base, exponent){
  output <- base^exponent
}
exponentialFunction(base = 4, exponent = 3)
```

There are other statements you can use to return some sort of output. For example, `print()` is another function that technically works, but they serve different purposes. The `print()` statement will print, on the screen, whatever value or character the object holds. However, it technically doesn't end the function. The `return()` statement, on the other hand, exits the function immediately.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
exponentialFunction1 <- function(base, exponent){
  output <- base^exponent
  print(output)
  output <- output + 2
  return(output)
}
exponentialFunction1(base = 4, exponent = 3)
exponentialFunction2 <- function(base, exponent){
  output <- base^exponent
  return(output)
  output <- output + 2
  return(output)
}
exponentialFunction2(base = 4, exponent = 3)
```

So here is another example of a user-defined function. However, this time, I'm going to use a function within a function to show you that anything in the curly brackets works so long as it has been loaded into your environment previously.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
standardDeviation <- function(vector){
  variance <- var(vector)
  return(sqrt(variance))
}
standardDeviation(rnorm(n = 10, mean = 0, sd = 1))
```

## 2.2 Defaults

Users can also set certain functions with defaults. We've already used a bunch of functions in the first and second part of the workshop. You've noticed that many times, there are more arguments necessary that we input into the function. That is because whoever wrote the function set certain arguments with defaults so that the function will still work if you do not specify the argument exactly. For example, we've been using `rnorm()` and specifying the mean and standard deviation arguments. However, the mean and standard deviation default is actually set to 0 and 1, respectively.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
x <- rnorm(n = 10000)
mean(x)
sqrt(var(x))
```

When writing your own function, setting defaults is very simple. So for the above example of the `exponentialFunction()`, let's set the base default to be 10, and the exponent default to be 1.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
exponentialFunction <- function(base = 10, exponent = 1){
  output <- base^exponent
  return(output)
}
exponentialFunction()
exponentialFunction(base = 4, exponent = 3)
```

## 2.3 Some More Examples
Functions can pretty much return whatever you want. For example, let's use the **airquality** dataset from the first part of the workshop. Say I want to return all the rows that have temperatures above 80.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
tempAbove80 <- function(data){
  above80 <- data[, 'Temp'] > 80
  newData <- data[above80, ]
  return(newData)
}
head(airquality)
head(tempAbove80(airquality))
```
## 2.4 Write Your Own Functions

So now we're going to try and write the two functions we need for this workshop. However, we understand that writing functions can be very difficult for beginners, so do not stress if you are not able to do this. This is just an exercise, and we admit that this following section can be quite difficult because it requires a compounded knowledge of everything we've done today.

## 2.4.1 Mapping Teams

Now you know the basics of writing a function. So let's start with the first function we want to write. We need to match up teams from [http://www.football-data.co.uk/](http://www.football-data.co.uk/) with teams from matchbook.com. The first thing to know is what the arguments are and what they look like. This function should have two arguments. We should have a dataframe with columns named "home.team" and "away.team" from matchbook.com. And the second argument should be a map of the team names from football data to matchbook.com. The mapping dataframe comes from the teams.csv file, so those columns are named "name" and "mb_name". You will definitely need to use the `match()` function. The `within()` and `with()` functions are useful, but are not necessary. (See Solutions)

```{r, include = FALSE}
# One way
map_teams <- function(fixtures, team_map){
	newFixtures <- fixtures
  newFixtures$home.team <- team_map$name[match(newFixtures$home.team, team_map$mb_name)]
	newFixtures$away.team <- team_map$name[match(newFixtures$away.team, team_map$mb_name)]
	return(newFixtures)
}

#Another way
map_teams <- function(fixtures, team_map){
	within(fixtures, {
		home.team <- with(team_map, name[match(home.team, mb_name)])
		away.team <- with(team_map, name[match(away.team, mb_name)])
	})
}
```

## 2.4.2 Getting English Premier League Games
The next function is to take the `event_data` and return just the Premier League games, reformatted in a way that is useable for our model predictions. The function we want can be split into two parts. 

#### Part 1: Returning Just Premier League Games

We want the first sub-objective to find only English Premier League games in the `event_data` dataframe. Begin by trying to write a function that returns only Premier League games. When trying to write functions with dataframes, it's important you learn the structure of the dataframe well. The `event_data` frame is actually more complex than it appears. First, you'll notice that the only way to determine Premier League games is in the meta-tags. Note that if you type in `meta-tags` without quotes around it, it will think you are saying to subtracts `tags` from `meta`. So to access the column, you might want to use:

```{r, include = FALSE}
username <- 'your_username' # You would eventually use your own username and password.
password <- 'your_password'
source('../settings.R')
session_details <- mb_login(username, password)
sport_id_data <- mb_get_sports(session_details)
football_sport_id <- with(sport_id_data, id[which(name=="Soccer")])
event_data <- mb_get_events(session_data = session_details, sport_ids = football_sport_id)
head(event_data)
```

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
event_data$`meta-tags`[1] # ` is the grave accent key
event_data[, "meta-tags"][1]
```

Now this should immediately stick out to you. The `event_data` dataframe is very complex. Look at the classes. We know that `event_data` is a dataframe, but then it looks like meta-tags column is actually a list. And within that list, each element is another dataframe.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
class(event_data)
class(event_data$`meta-tags`)
class(event_data$`meta-tags`[[1]])
```

So to return only Premier League games can actually be quite tricky. Name the function `get_prem_games()` and try it yourself before revealing the solution. There will be many ways to do this. Some useful functions you may want to consider are `subset()`, `grepl()` and/or `which()`. As a hint, here is what a couple of lines of just Premier League games looks like.

```{r, collapse = TRUE, echo = FALSE, comment = ""}
head(subset(event_data, grepl('England', event_data$`meta-tag`) & grepl('Premier', event_data$`meta-tag`)), 3)
```

And here's what just the meta-tags of the above three lines looks like:

```{r, collapse = TRUE, echo = FALSE, comment = ""}
head(subset(event_data, grepl('England', event_data$`meta-tag`) & grepl('Premier', event_data$`meta-tag`))$`meta-tags`, 3)
```

Note the second row is NOT a row we want in the final output. Though it is Premier League, it does not actually have any information about who played. (See Solutions)

```{r, include = FALSE}
get_prem_games <- function(event_data){
	prem <- grepl('Premier', event_data$`meta-tag`) & 
		grepl('England', event_data$`meta-tag`) & 
		grepl(' vs ', event_data$name)
	
	return(event_data[prem, ])
}
```

#### Part 2: Reformatting the Output

We now want the data to be returned in a certain format. Remember the things we need to make predictions? We need home.team, away.team, home.adv, and away.adv. So those are crucial. But we also want a couple of more things. We want a column for away.score and home.score, which in this case, will be `NaN` because in practice, these games will not have happened yet. We want the date and the ID number so that we can identify them easily in the future. So eventually, we want the data to look like the following:

```{r, collapse = TRUE, echo = FALSE, comment = ""}
get_prem_games <- function(event_data){
	prem <- grepl('Premier', event_data$`meta-tag`) & 
		grepl('England', event_data$`meta-tag`) & 
		grepl(' vs ', event_data$name)

	with(event_data[prem,], {
		teams <- as.data.frame(
			do.call('rbind', strsplit(name, ' vs ')),
			stringsAsFactors=FALSE
		)
		colnames(teams) <- c('home.team', 'away.team')
		within(teams, {
			id <- id
			away.adv <- 'away'
			home.adv <- 'home'
			date <- as.Date(start)
			home.score <- NaN
			away.score <- NaN
		})
	})
}
head(get_prem_games(event_data))
```

You can do this in many ways, but some useful functions are `with()`, `within()`, `rbind()`, `cbind()`, `strsplit()`, and `do.call()`. So extend the previous `get_prem_games()` to return the reformatted data. (See Solutions)

```{r, include = FALSE}
# One way
get_prem_games <- function(event_data){
	prem <- grepl('Premier', event_data$`meta-tag`) & 
		grepl('England', event_data$`meta-tag`) & 
		grepl(' vs ', event_data$name)
	
	new_event_data <- event_data[prem, ]
	teams <- as.data.frame(
			do.call('rbind', strsplit(new_event_data$name, ' vs ')),
			stringsAsFactors=FALSE
		)
	
	final_event_data <- cbind(teams, new_event_data$id, as.Date(new_event_data$start))
	colnames(final_event_data) <- c("home.team", "away.team", "id", "date")
	final_event_data$away.adv <- "away"
	final_event_data$home.adv <- "home"
	final_event_data$home.score <- final_event_data$away.score <- NaN
	
	return(final_event_data)
}

# Another way
get_prem_games <- function(event_data){
	prem <- grepl('Premier', event_data$`meta-tag`) & 
		grepl('England', event_data$`meta-tag`) & 
		grepl(' vs ', event_data$name)

	with(event_data[prem,], {
		teams <- as.data.frame(
			do.call('rbind', strsplit(name, ' vs ')),
			stringsAsFactors=FALSE
		)
		colnames(teams) <- c('home.team', 'away.team')
		within(teams, {
			id <- id
			away.adv <- 'away'
			home.adv <- 'home'
			date <- as.Date(start)
			home.score <- NaN
			away.score <- NaN
		})
	})
}
```

# 3 Finding Value

## 3.1 Making Predictions

Now that we've created the necessary functions, we now want to make predictions. We'll begin by actually using the previous functions to get the data into the proper format. Don't forget, we first need to read in the team.csv file for the team mapping.

```{r, eval = FALSE, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
teams <- read.csv('./data/team.csv', stringsAsFactors=FALSE)
prem <- get_prem_games(event_data)
prem <- map_teams(prem, team_map = teams)
```

```{r, include = FALSE}
teams <- read.csv('../data/team.csv', stringsAsFactors=FALSE)
prem <- get_prem_games(event_data)
prem <- map_teams(prem, team_map = teams)
```

Next, we need to load in our previous model that we saved. If you remember, to save the model we used the `saveRDS()` function. So to load in the model, we'll be using the `loadRDS()` function. So load in the model, and make predictions. 

```{r, eval = FALSE, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
model_fit <- readRDS('./output/model_fit.rds')
predictions <- predict(model_fit, newdata=prem)
```

```{r, include = FALSE}
model_fit <- readRDS('../output/model_fit.rds')
predictions <- predict(model_fit, newdata=prem)
```

Get to know the structure of the output. You can see that it's a list. We're solely interested in the first dataframe in the list, which is the `scores` dataframe. We're just going to overwrite the `prem` object with the `predictions$scores` dataframe since if you examine it, the `predictions$scores` dataframe is the `scores` dataframe with additional information.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
prem <- predictions$scores
```

## 3.2 Getting the Target Price

This next step is quite simple. All we need to do is get the target price. To do that, we first need to see how the probabilities are being reported. Since the output of the prediction appears to report the home.win, away.win, and tie in percentages, and because our target price is in decimal odds, we simply divide 100 by the percentage probability. We're also going to create `NA` columns for away.price, tie.price, and home.price for when we retrieve the prices.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
prem <- within(prem, {
	home.target <- 100 / home.win
	tie.target <- 100 / tie
	away.target <- 100 /away.win
	home.price <- NA
	tie.price <- NA
	away.price <- NA
})
```

## 3.3 Retrieving Live Prices

As we said earlier, we created empty columns for away.price, tie.price, and home.price for when we actually retrieve the prices. We need these so we can determine if there is value on the bet. So to do that, we need to retrieve it from the website. Let's try and retrieve prices for just the first game. Again, if you've read the "Quickstart Tutorial", you should be able to do this. If you need a refresher, please return to the tutorial.

So to retrieve the price, we need the ID. We then will use the `mb_get_markets()` function to get the market data.

```{r, eval = FALSE, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
event_id <- prem[1, 'id']

market_data <- mb_get_markets(
	session_data = session_details,
	event_id = event_id,
	include_runners = TRUE
)
```

We then need the market ID number to identify the exact bet we are looking for. Again, like the `event_data` dataframe, the structure of this dataframe is quite complex.

```{r, eval = FALSE, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
market_id <- market_data$id[market_data$name == 'Match Odds']
```

Now that we have the exact market ID, we need to retrieve all the runners in this market. To do that, we'll need to use the `mb_get_runners()` function.

```{r, eval = FALSE, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
runner_data <- mb_get_runners(
	session_data = session_details,
	event_id = event_id,
	market_id = market_id,
	include_prices = TRUE
)
```

We're almost there. Now, we want to find the maximum backing bet in decimal odds. We want to then assign the price to the corresponding home.price, away.price, and tie.price in the `prem` dataset.

```{r, eval = FALSE, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
prem[1, 'home.price'] <- max(subset(runner_data[1, 'prices'][[1]], side=='back', `decimal-odds`))
prem[1, 'tie.price'] <- max(subset(runner_data[2, 'prices'][[1]], side=='back', `decimal-odds`))
prem[1, 'away.price'] <- max(subset(runner_data[3, 'prices'][[1]], side=='back', `decimal-odds`))
```

### 3.3.1 For Loops
So now we've found the price for the first game in our dataset. We need to do the process again for every game in our dataset. Now, this may seem like a daunting task, but the whole point of computing is to automate menial tasks like this one. So we've shown you how to find the price for a single game. Now we can make R loop the same process for every single game. For loops are quite easy to grasp. Remember how we isolated the event id? We used `event_id <- prem[1, 'id']`. Well, we can replace that 1 with a stand-in variable. We can then use the for syntax to loop through. So here's a simple example.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
for(i in 1:10){
  print(i)
}
```

So you can see that `i` becomes a stand-in for any number that we've told the R to loop through. So can you do the same thing for the above code? (See Solutions)

```{r, include = FALSE}
for (i in 1:nrow(prem)){
	event_id <- prem[i, 'id']

	market_data <- mb_get_markets(
		session_data = session_details,
		event_id = event_id,
		include_runners = TRUE
	)

	market_id <- market_data$id[market_data$name == 'Match Odds']

	runner_data <- mb_get_runners(
		session_data = session_details,
		event_id = event_id,
		market_id = market_id,
		include_prices = TRUE
	)


	prem[i, 'home.price'] <- max(subset(runner_data[1, 'prices'][[1]], side=='back', `decimal-odds`))
	prem[i, 'tie.price'] <- max(subset(runner_data[2, 'prices'][[1]], side=='back', `decimal-odds`))
	prem[i, 'away.price'] <- max(subset(runner_data[3, 'prices'][[1]], side=='back', `decimal-odds`))

}
```

## 3.4 Finding Value on a Bet

The very last thing is to find value on the bet. And that is incredibly simple. We just want to determine when the price is greater than our determined target price. We can do that within in the `prem` dataframe. We can use the `ifelse()` function to do that, which, if you've used excel, operates the same way as the if function in excel.

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
prem <- within(prem, {
	target <- ifelse(home.price > home.target, 'home',
		ifelse(tie.price > tie.target, 'tie',
		ifelse(away.price > away.target, 'away', NA)))
})

prem[, c('home.team', 'away.team', 'date', 'target')]
```

\newpage

# Solutions

#### Logging Into Matchbook.com and Getting Football Games

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
username <- 'rworkshop' # You would eventually use your own username and password.
password <- 'back0fnet'
session_details <- mb_login(username, password)
sport_id_data <- mb_get_sports(session_details)
football_sport_id <- with(sport_id_data, id[which(name=="Soccer")])
event_data <- mb_get_events(session_data = session_details, sport_ids = football_sport_id)
head(event_data)
```

#### Mapping Teams

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
# One way
map_teams <- function(fixtures, team_map){
	newFixtures <- fixtures
  newFixtures$home.team <- team_map$name[match(newFixtures$home.team, team_map$mb_name)]
	newFixtures$away.team <- team_map$name[match(newFixtures$away.team, team_map$mb_name)]
	return(newFixtures)
}

#Another way
map_teams <- function(fixtures, team_map){
	within(fixtures, {
		home.team <- with(team_map, name[match(home.team, mb_name)])
		away.team <- with(team_map, name[match(away.team, mb_name)])
	})
}
```

#### Returning Just Premier League Games

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
get_prem_games <- function(event_data){
	prem <- grepl('Premier', event_data$`meta-tag`) & 
		grepl('England', event_data$`meta-tag`) & 
		grepl(' vs ', event_data$name)
	
	return(event_data[prem, ])
}
```

#### Reformatting the Output

```{r, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
# One way
get_prem_games <- function(event_data){
	prem <- grepl('Premier', event_data$`meta-tag`) & 
		grepl('England', event_data$`meta-tag`) & 
		grepl(' vs ', event_data$name)
	
	new_event_data <- event_data[prem, ]
	teams <- as.data.frame(
			do.call('rbind', strsplit(new_event_data$name, ' vs ')),
			stringsAsFactors=FALSE
		)
	
	final_event_data <- cbind(teams, new_event_data$id, as.Date(new_event_data$start))
	colnames(final_event_data) <- c("home.team", "away.team", "id", "date")
	final_event_data$away.adv <- "away"
	final_event_data$home.adv <- "home"
	final_event_data$home.score <- final_event_data$away.score <- NaN
	
	return(final_event_data)
}

# Another way
get_prem_games <- function(event_data){
	prem <- grepl('Premier', event_data$`meta-tag`) & 
		grepl('England', event_data$`meta-tag`) & 
		grepl(' vs ', event_data$name)

	with(event_data[prem,], {
		teams <- as.data.frame(
			do.call('rbind', strsplit(name, ' vs ')),
			stringsAsFactors=FALSE
		)
		colnames(teams) <- c('home.team', 'away.team')
		within(teams, {
			id <- id
			away.adv <- 'away'
			home.adv <- 'home'
			date <- as.Date(start)
			home.score <- NaN
			away.score <- NaN
		})
	})
}
```

#### For Loop

```{r, eval = FALSE, collapse = TRUE, echo = TRUE, comment = "", prompt = TRUE}
for (i in 1:nrow(prem)){
	event_id <- prem[i, 'id']

	market_data <- mb_get_markets(
		session_data = session_details,
		event_id = event_id,
		include_runners = TRUE
	)

	market_id <- market_data$id[market_data$name == 'Match Odds']

	runner_data <- mb_get_runners(
		session_data = session_details,
		event_id = event_id,
		market_id = market_id,
		include_prices = TRUE
	)


	prem[i, 'home.price'] <- max(subset(runner_data[1, 'prices'][[1]], side=='back', `decimal-odds`))
	prem[i, 'tie.price'] <- max(subset(runner_data[2, 'prices'][[1]], side=='back', `decimal-odds`))
	prem[i, 'away.price'] <- max(subset(runner_data[3, 'prices'][[1]], side=='back', `decimal-odds`))

}
```
