# dc_gambleR #

This repository contains the material used during the MatchBook workshop on R: Download football data, fit a probabilistic model, make predictions and place bets on [MatchBook](www.matchbook.com).

### Content ###


The aim is to give a brief introduction to the fundamentals of R (handout 1).

Then to download all the results from the current English Premier League season and fit a variation of the Dixon and Coles (1997) models to make predictions for future matches - handout 2.

Finally, handout 3 shows how to use the MatchBook R package to identify value bets.

Within the source the following resources are available:

* Slides used during the presentation.
* Handouts 1, 2 and 3 with hands-on work.
* R code with an implementation _solutions_
* Data and external resources

### How do I get set up? ###

Install the following software and R packages:

* [R](https://cran.r-project.org/)
* [Rstudio](https://www.rstudio.com/)
* [git](https://git-scm.com/)
* Packages: [fbRanks](https://cran.r-project.org/web/packages/fbRanks/index.html), devtools, matchbook.