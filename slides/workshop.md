R Workshop
========================================================
---
author: Luigi Colombo
date: 8th December 2015
transition: fade
navigation: section
output:
  revealjs::revealjs_presentation:
    pandoc_args: [ "--slide-level", "2" ]
---

Luigi Colombo

8th December 2015

Introduction
========================================================
type: prompt
incremental: true

- Who am I
- What are we going to cover:

> R code to fit a football model and interface with Matchbook

- What are we NOT going to cover:

> In depth statistical modelling, in depth programming features

Content
========================================================
type: section

Handout 1:
- What is R and why (pros and cons)
- Rstudio and github
- data.frame, read.csv

Handout 2:
- football model
- packages, fbRanks

---

Handout 3:
- matchbook API
- extensions

What is R and why (pros and cons)
========================================================
type: subsection

> R is a dynamic language for statistical computing that combines lazy functional features and object-oriented programming"

In practice it is easy to learn how to do something in R, but it is really hard to learn R well.


What is R and why (pros and cons)
========================================================
type: subsection
incremental: true

pros:

- lots of statistical models available
- quick to analyse data
- plotting and graphical features
- interactive

___

cons:

- ecosystem to support development - improving a lot
- many different ways of doing the same thing (objected-oriented, functional, dynamic, lazy evaluation...)
- it is very forgiving


Rstudio and git
========================================================
type: section

* Rstudio is a powerful IDE to help with developing R code. It is for many the easiest way to get started with R: https://www.rstudio.com/

* _git_ is a distributed version control system. If you are serious about coding and developing anythine meaningful in any language, _git_ is a necessary tool.

* There are various web-based git repository hosting services to share your projects: 
   [github](https://github.com)
  ~ [bitbucket](https://bitbucket.org)
  ~ [gitlab](https://gitlab.org)

Let's get started
========================================================
type: section

1. Install R, Rstudio, git
1. Tools -> Global options -> Git/SVN -> Git executable: Programs\Git\bin\git.exe
1. Create a new project:
 * version control
 * git -> https://bitbucket.org/ggcolombo/dc_gambler.git
1. Everything you need:
 * data
 * handouts
 * slides: this very slides
 * R: files with the _solutions_


Handout 1
========================================================
type: section

* Introduction to R.
* Set up a project
* R `data.frame` and other data structure.
* Loading and playing with some data.

Handout 2
========================================================
type: section

Let's build a football model and use historical data to estimate teams' strength and make predictions for upcoming matches.

We'll loosely follow the Dixon and Coles (1997) seminal paper and implement an independent Poisson model with a time discount.

football model
========================================================
type: subsection


$$
\log(\lambda_h) = \alpha_i + \beta_j + \eta \\
\log(\lambda_a) = \alpha_j + \beta_i - \eta \\
p(x,y| \lambda_h, \lambda_a) \approx \exp(-\lambda_h) \lambda_h^x \exp(-\lambda_a)\lambda_a^y\\
nll(\theta| X) = \sum_{k=1}^n \big(\lambda_{h(i)}+\lambda_{a(i)} -x_i\log(\lambda_{h(i)}) -y_i\log(\lambda_{a(i)})\big)^{-\phi t_i}
$$

* $\alpha$ and $\beta$ capture the attacking and defending strenght of each team.
* $\eta$ is a home advantage

football model
========================================================
type: subsection

We can go about it in two ways:

* write the model from scratch (more flexibility, better understanding; much longer and higher risk of bugs).
* re-use someone else's work! Extend it and contribute back. 

> install.packages('fbRanks')
>
> vignette('fbRanks')


Handout 3
========================================================

MatchBook has an API (application programming interface) to be used to build your own software to interface with the MatchBook exchange.

Helpfully MatchBook also provides an R client which makes writing the code in R very easy.:

https://github.com/xanadunf/matchbook

> install.packages('devtools')
>
> library(devtools)
>
> install_github("xanadunf/matchbook")




References
========================================================

- https://cran.r-project.org/
